import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = "app";

  // json from https://mrsoft.by/tz20/list.json
  response: any;

  // json from https://mrsoft.by/tz20/cats/{id}.json
  infoAboutItem: any;

  server: string = 'http://localhost:7000/info/';
  resource: string = 'https://mrsoft.by/tz20/list.json'


  selectedItem: Item = { id: '', name: '', shortInfo: '', picture: '', bio: ''};

  // text in search line
  searchVar: string = '';

  // .data array
  array: string[];

  // the item is clicked?
  state: any[] = [];

  constructor (private http: HttpClient) { }
 
  getJson () {
    this.http.get(this.server + this.resource)
    .subscribe((response) => {
      this.response = response;
      this.array = this.response.data
    })
  }

  addState () {
    setTimeout(() => {
      for (let value of this.response.data) {
        this.state.push(false)
      }
      console.log(this.state)
    }, 100);
  }

  returnPositionOfItem (id: string) {
    for (var i = 0; i < this.response.data.length; i++) {
      if (this.response.data[i].id == id) {
        return i;
      }
    }
  }

  returnState (id: string) {
    this.state = eval('[' + localStorage.getItem('state') + ']') 
    return this.state[this.returnPositionOfItem(id)];
  }

  changeState (name: string, shortInfo: string, id: string) {
    var position = this.returnPositionOfItem(id)
    this.state[position] = !this.state[position];
    
    localStorage.removeItem('state')
    localStorage.setItem('state', this.state.toString()) 

    if (this.state[position] && this.selectedItem.id == id) {
      this.selectedItem.name = null;
      this.selectedItem.shortInfo = null;
      this.selectedItem.bio = null;
      this.selectedItem.picture = null;
      localStorage.removeItem('selectedItem')
    }
    if (!this.state[position] && this.selectedItem.id == id || !this.state[position] && this.selectedItem.id == '') {
      setTimeout(() => {
        console.log(id)
        this.getInfoById(name, shortInfo, id, undefined);
      }, 100);
    } 
  }

  getInfoById (name: string, shortInfo: string, id: string, time) {
    setTimeout(() => {
      this.http.get(this.server + this.response.basepath + this.response.data[this.returnPositionOfItem(id)].more)
      .subscribe((response) => {
        this.infoAboutItem = response;

        this.selectedItem = {
          id: id,
          name: name,
          shortInfo: shortInfo,
          picture: this.response.basepath + this.infoAboutItem.pic,
          bio: this.infoAboutItem.bio
        } 

        localStorage.setItem('selectedItem', JSON.stringify(this.selectedItem))
      })
    }, time);
  }

  searchByName () {
    this.array = this.response.data.filter(res => {
      return res.name.toLocaleLowerCase().match(this.searchVar.toLocaleLowerCase());
    })
  }

  ngOnInit () {
    this.getJson()
    this.selectedItem = JSON.parse(localStorage.getItem('selectedItem'))
    console.log(this.selectedItem)
    if (this.selectedItem == null) {
      this.selectedItem = { id: '', name: '', shortInfo: '', picture: '', bio: ''}
    } else {
      this.getInfoById(this.selectedItem.name, this.selectedItem.shortInfo, this.selectedItem.id, 200)
    }
    console.log(this.selectedItem)
    this.addState()
  }

}

interface Item {
  id: string;
  name: string;
  shortInfo: string;
  picture: any;
  bio: string;
}