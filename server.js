const { connectableObservableDescriptor } = require('rxjs/internal/observable/ConnectableObservable');

const express = require('express'),
  app = express(),
  request = require('request')

const port = 7000

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(express.static(process.cwd()+"/dist/http-get/"));

app.get('/', (req, res) => {
    res.sendFile(process.cwd()+"/dist/http-get/index.html")
});

app.get('/info/*', (req, res) => {
  request(req.url.replace('/info/', ''), (err, response, body) => {
    if (err) return res.status(500).send({ message: err })
    return res.send(body)
  })
})

app.listen(port, () => console.log(`Server listens http://localhost:${port}`))